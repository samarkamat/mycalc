

#enable_testing()

add_executable (blah-test blah-test.cpp)

target_link_libraries (blah-test ${GTEST_BOTH_LIBRARIES})

install (TARGETS blah-test 
    RUNTIME DESTINATION ${CMAKE_INSTALL_PREFIX}/bin)

