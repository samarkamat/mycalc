#include <gtest/gtest.h>
#include <iostream>


TEST(sample_test_case, sample_test)
{
    EXPECT_EQ(1, 1);
}



int main (int argc, char** argv)
{
    std::cout << "starting tester\n";
    testing::InitGoogleTest(&argc, argv);
    RUN_ALL_TESTS();
    return 1;
}
