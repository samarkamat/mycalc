# MYCALC
This is a proof-of-concept interpreter of mathematical expressions. Having the expressions defined in a separate functional language allows other non-developer users to read, review and modify the expressions without understanding C++.

## Building
$ ls  
  mycalc  
$ mkdir build install  
$ cd build  
$ cmake -DCMAKE_INSTALL_PREFIX=../install ../mycalc/src  
$ make install  

## Running
This will be updated when there's something to run
